// Calculer et retourner la factorielle de n.
// Par convention, factorielle(0) == 1.
function factorielle(n) {

};

// Vérifier si un le nombre donné est un nombre premier.
// Si oui, retourner vrai, sinon faux.
// Un nombre premier est un nombre divisible uniquement par 1 et par lui-même.
function nombrePremier(nombre) {

};

// Multiplier 2 nombres sans utiliser la multiplication et retourner la réponse.
function multiplier(nombre1, nombre2) {

};

// Calculer et retourner le miroir d'un nombre. Le miroir d'un nombre est obtenu
// en lisant le nombre à l'envers.
// Par exemple, le miroir de 4209 est 9024.
function miroir(nombre) {

};
